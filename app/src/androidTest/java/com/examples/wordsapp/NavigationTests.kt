package com.examples.wordsapp

import androidx.fragment.app.testing.launchFragmentInContainer
import androidx.navigation.Navigation
import androidx.navigation.testing.TestNavHostController
import androidx.recyclerview.widget.RecyclerView
import androidx.test.core.app.ApplicationProvider
import androidx.test.espresso.Espresso.onView
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.contrib.RecyclerViewActions
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.runner.AndroidJUnit4
import com.example.wordsapp.LetterListFragment
import com.example.wordsapp.R
import org.junit.Assert.assertEquals
import org.junit.Before
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4::class)
class NavigationTests {
    private lateinit var navController: TestNavHostController

    @Before
    fun setup(){
        // create a test instance of the navigation controller
        navController = TestNavHostController(
            ApplicationProvider.getApplicationContext()
        )
        //Here we specify that we want to launch the LetterListFragment.
        //We have to pass the app's theme so that the UI components know which theme to use or the test may crash.
        val letterListScenario = launchFragmentInContainer<LetterListFragment>(themeResId =
        R.style.Theme_Words)
        //explicitly declare which navigation graph we want the nav controller to use for the fragment launched.
        letterListScenario.onFragment { fragment ->
            navController.setGraph(R.navigation.nav_graph)
            Navigation.setViewNavController(fragment.requireView(), navController)
        }
    }

    @Test
    fun navigate_to_words_nav_component(){
        //Now trigger the event that prompts the navigation
        onView(withId(R.id.recycler_view))
            .perform(
                RecyclerViewActions
                .actionOnItemAtPosition<RecyclerView.ViewHolder>(2, click()))
        //we can check to make sure that the current navigation controller's destination has the ID of the fragment we expect to be in
        assertEquals(navController.currentDestination?.id, R.id.wordListFragment)
    }
}
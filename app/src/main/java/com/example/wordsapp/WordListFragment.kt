package com.example.wordsapp

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.wordsapp.databinding.FragmentWordListBinding

class WordListFragment : Fragment() {

    //it should be nullable, because you can't inflate the layout until onCreateView() is called.
    private var _binding: FragmentWordListBinding? = null
    //If you're certain a value won't be null when you access it, you can append !! to its type name.
    private val binding get() = _binding!!
    //You can mark this as lateinit so that you don't have to make it nullable.
    private lateinit var letterId: String

    //create companion object using singleton pattern - it will be associated with DetailActivity
    companion object {
        //to pass the clicked letter button to detailed view
        const val LETTER = "letter"
        //base URL for a Google search
        const val SEARCH_PREFIX = "https://www.google.com/search?q="
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)

        //Because it's possible for arguments to be optional, notice you call let() and pass in a lambda.
        //This code will execute assuming arguments is not null, passing in the non null arguments for the it parameter.
        //If arguments is null, however, the lambda will not execute.
        arguments?.let {
            letterId = it.getString(LETTER).toString()
        }
    }
    //Implement onCreateView() by inflating the view, setting the value of _binding, and returning the root view.
    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        _binding = FragmentWordListBinding.inflate(inflater, container, false)
        val view = binding.root
        return view
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        // Retrieve the LETTER from the Intent extras
        // intent.extras.getString returns String? (String or null)
        // so toString() guarantees that the value will be a String
        //val letterId = "A"
        // intent is a property of any activity. It keeps a reference to the intent used to launch it
        //for fragments you need to use activity.intent

        val recyclerView = binding.recyclerView
        recyclerView.layoutManager = LinearLayoutManager(requireContext())
        recyclerView.adapter = WordAdapter(letterId, requireContext())

        // Adds a [DividerItemDecoration] between items
        recyclerView.addItemDecoration(
            DividerItemDecoration(context, DividerItemDecoration.VERTICAL)
        )

        //title = getString(R.string.detail_prefix) + " " + letterId
    }

    override fun onDestroyView() {
        super.onDestroyView()
        //it is required to reset the _binding to null
        _binding = null
    }


}